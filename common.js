const startButton = document.getElementById("startButton");
const cancelButton = document.getElementById("cancelButton");

let timer;

function hideImages() {
    const imagesDisplays = document.getElementsByClassName('images-wrapper');
  
    for (const imagesDisplay of imagesDisplays) {
      const imageElements = imagesDisplay.getElementsByTagName('img');
      for (let i = 1; i < imageElements.length; i++) {
        imageElements[i].style.display = 'none';
      }
    }
  }
  
  function displayImages() {
    const imagesDisplays = document.getElementsByClassName('images-wrapper');
  
    for (const imagesDisplay of imagesDisplays) {
      const currentImage = parseInt(imagesDisplay.getAttribute('data-img'));
      let nextImage;
  
      switch (currentImage) {
        case 1:
          nextImage = 2;
          break;
        case 2:
          nextImage = 3;
          break;
        case 3:
          nextImage = 4;
          break;
        case 4:
          nextImage = 1;
          break;
        default:
          nextImage = 1;
          break;
      }
  
      imagesDisplay.setAttribute('data-img', nextImage);
  
      const imageElement = imagesDisplay.querySelector('img');
  
      imageElement.src = `./img/${nextImage}.jpg`;
      imageElement.style.display = 'block'; 
    }
  }
  
  timer = setInterval(displayImages, 3000);
  displayImages();
  hideImages();
 

function cancelTimer() {
    clearTimeout(timer);
    timer = null;
}
function startTimer() {
  if (!timer) {
    timer = setInterval(displayImages, 3000);
  }
}

startButton.addEventListener("click", startTimer);
cancelButton.addEventListener("click", cancelTimer);


